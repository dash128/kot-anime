package pe.com.dash.kot_anime.ui.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import pe.com.dash.kot_anime.R
import pe.com.dash.kot_anime.Repositories.AnimeRepository
import pe.com.dash.kot_anime.adapters.AnimeAdapter
import pe.com.dash.kot_anime.ui.models.Anime
import pe.com.dash.kot_anime.ui.viewmodels.AnimeViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var animeViewModel: AnimeViewModel
    private lateinit var animeAdapter: AnimeAdapter
    private lateinit var animes: MutableList<Anime>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViewModel()
        setupUI()

    }

    fun setupUI(){
        animeAdapter = AnimeAdapter(animeViewModel.getAnimeLiveData().value?: emptyList(), this)
        rvMainAnime.layoutManager = LinearLayoutManager(this)
        rvMainAnime.adapter = animeAdapter
    }

    fun setupViewModel(){
        animeViewModel = ViewModelProviders.of(this).get(AnimeViewModel::class.java)
        animeViewModel.getAnimeLiveData().observe(this,animeObserver)
    }

    private val animeObserver = Observer<List<Anime>>{
        animeAdapter.update(it)
    }

}
