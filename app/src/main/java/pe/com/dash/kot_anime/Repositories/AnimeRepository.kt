package pe.com.dash.kot_anime.Repositories


import android.util.Log
import androidx.lifecycle.MutableLiveData
import pe.com.dash.kot_anime.db.retrofit.IAnimeDashAPI
import pe.com.dash.kot_anime.ui.models.Anime
import pe.com.dash.kot_anime.util.Coroutines
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AnimeRepository {

    fun getAnimes(): List<Anime> {
        return listOf(
            Anime("11","nombre11","descripcion1","imagen1", "link1"),
            Anime("21","nombre21","descripcion2","imagen2", "link2"),
            Anime("31","nombre31","descripcion3","imagen3", "link3"),
            Anime("41","nombre41","descripcion4","imagen4", "link4"),
            Anime("51","nombre51","descripcion5","imagen5", "link5")
        )

    }

    /*fun getAnimesFromRetrofir(): MutableLiveData<List<Anime>> {
        val result = MutableLiveData<List<Anime>>()
        IAnimeDashAPI.create().getAllAnimes().enqueue(object: Callback<MutableList<Anime>>{
            override fun onFailure(call: Call<MutableList<Anime>>, t: Throwable) {
                Log.d("MILAAAAA-->", t.toString())
            }

            override fun onResponse(call: Call<MutableList<Anime>>, response: Response<MutableList<Anime>>) {
                if (response.isSuccessful){
                    Log.d("IFFFFF-->", "isSuccessful")

                    result.value = response.body()
                }
            }
        })

        return result
    }*/

    suspend fun getAnimeCorritines(): Response<MutableList<Anime>> {
        return IAnimeDashAPI.create().getAllAnimes()
    }

    fun getAnimeRetrofit(): List<Anime>? {
        val result = MutableLiveData<List<Anime>>()
        Coroutines.main{
            val res = IAnimeDashAPI.create().getAllAnimes()
            if(res.isSuccessful){
                result.value = res.body()
            }
        }

        return result.value
    }
}