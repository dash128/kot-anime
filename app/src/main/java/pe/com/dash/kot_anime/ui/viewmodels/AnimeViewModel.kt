package pe.com.dash.kot_anime.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineStart

import pe.com.dash.kot_anime.Repositories.AnimeRepository
import pe.com.dash.kot_anime.ui.models.Anime
import pe.com.dash.kot_anime.util.Coroutines

class  AnimeViewModel: ViewModel() {
    val animeRepository = AnimeRepository()
    var animes = MutableLiveData<List<Anime>>()

    init {
        this.loadAnimes2()
    }


    private fun loadAnimes(){
        animes.value = animeRepository.getAnimeRetrofit()
    }
    private fun loadAnimes2(){
        Coroutines.main{
            val response = animeRepository.getAnimeCorritines()
            if(response.isSuccessful){
                animes.value = response.body()
            }
        }

        //animes.value = animeRepository.getAnimes()
        //animes.value = animeRepository.getAnimesFromRetrofir().value
    }
    fun getAnimeLiveData():LiveData<List<Anime>>{
        return animes
    }

}