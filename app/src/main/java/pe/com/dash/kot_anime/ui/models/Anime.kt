package pe.com.dash.kot_anime.ui.models

import com.google.gson.annotations.SerializedName

data class Anime(

    @SerializedName("_id")
    val Id: String,


    @SerializedName("nombre")
    val Nombre: String,


    @SerializedName("descripcion")
    val Descripcion: String,


    @SerializedName("imagen")
    val Imagen: String,


    @SerializedName("link")
    val Link: String
)