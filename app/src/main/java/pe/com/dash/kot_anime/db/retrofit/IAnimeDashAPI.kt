package pe.com.dash.kot_anime.db.retrofit

import pe.com.dash.kot_anime.ui.models.Anime
import retrofit2.Call
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface IAnimeDashAPI {

    @GET("anime")
    suspend fun getAllAnimes(): Response<MutableList<Anime>>

    companion object{
        private const val BASE_URL = "http://192.168.0.6:3000/api/"

        fun create(): IAnimeDashAPI{
            val retrofit = retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(IAnimeDashAPI::class.java)
        }
    }


}