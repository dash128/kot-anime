package pe.com.dash.kot_anime.adapters

import android.app.Activity
import android.graphics.Point
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.prototype_anime.view.*
import pe.com.dash.kot_anime.R
import pe.com.dash.kot_anime.ui.models.Anime

class AnimeAdapter(var animes: List<Anime>, var activity: Activity): RecyclerView.Adapter<AnimePrototype>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimePrototype {
        var view: View = LayoutInflater.from(parent.context).inflate(R.layout.prototype_anime, parent, false)
        val myPrototype: AnimePrototype = AnimePrototype(view)
        return  myPrototype
    }

    override fun getItemCount(): Int { return animes.size }

    override fun onBindViewHolder(holder: AnimePrototype, position: Int) {
        holder.bind(animes[position])
    }

    fun update(data: List<Anime>){
        animes = data
        notifyDataSetChanged()
    }
}

class AnimePrototype(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private var tvAnimeName = itemView.tvAnimeName
    private var ivAnimePicture: ImageView = itemView.ivAnimePicture
    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

    }

    fun bind(anime: Anime){
        tvAnimeName.text = anime.Nombre
        val picasso = Picasso.get()

        picasso.load(anime.Imagen)
            .into(ivAnimePicture)
    }
}
